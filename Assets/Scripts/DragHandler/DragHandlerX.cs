using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
 
public class DragHandlerX : MonoBehaviour, IDragHandler, IEndDragHandler 
{
    public GameObject item1, item2, item3, item4, dragObject, target1, target2, benar1, salah1, benar2, salah2, lanjutkan;
	bool item1target1, item1target2, item4target1, item4target2;
    Vector3 startpos, startpos1, startpos2, startpos3, startpos4;

    void Start()
	{
		item1target1 = false;
		item1target2 = false;
		item4target1 = false;
		item4target2 = false;
		startpos = dragObject.transform.position;
        startpos1 = item1.transform.position;
		startpos2 = item2.transform.position;
		startpos3 = item3.transform.position;
        startpos4 = item4.transform.position;
    }

    public void OnDrag(PointerEventData eventData)
	{
   		if (!item1target1 && item4target2)
		{
	        Vector3 newpos = Input.mousePosition - new Vector3 (Screen.width / 2f, Screen.height / 2f, 0f);
	        dragObject.transform.localPosition = newpos;
		}
		else if (!item4target1 && item1target2)
		{
	        Vector3 newpos = Input.mousePosition - new Vector3 (Screen.width / 2f, Screen.height / 2f, 0f);
	        dragObject.transform.localPosition = newpos;
		}
		else if (!item1target1 && !item4target1)
		{
	        Vector3 newpos = Input.mousePosition - new Vector3 (Screen.width / 2f, Screen.height / 2f, 0f);
	        dragObject.transform.localPosition = newpos;
		}
    }
 
    public void OnEndDrag(PointerEventData eventData)
	{
        float Dis1 = Vector3.Distance(dragObject.transform.position, target1.transform.position);
        float Dis2 = Vector3.Distance(dragObject.transform.position, target2.transform.position);

		if (!item1target1 && item4target2)
		{
	        if(Dis1<2)
			{
	            dragObject.transform.position = target1.transform.position;

				if (target1.transform.position == item1.transform.position)
				{
			        actionBenar(1);
					item2Back();
					item3Back();
					item1target1 = true;
					target1.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
			    }
				else if (target1.transform.position == item2.transform.position)
				{
			        actionSalah(1);
					item1Back();
					item3Back();
			    } 
				else if (target1.transform.position == item3.transform.position)
				{
			        actionSalah(1);
					item1Back();
					item2Back();
			    }
	        }
			else 
			{
				dragObject.transform.position = startpos;
	        }
	    }
		else if (!item4target1 && item1target2)
		{
	        if(Dis1<2)
			{
	            dragObject.transform.position = target1.transform.position;

				if (target1.transform.position == item4.transform.position)
				{
					actionBenar(1);
					item2Back();
					item3Back();
					item4target1 = true;
					target1.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
			    }
				else if (target1.transform.position == item2.transform.position)
				{
			        actionSalah(1);
					item4Back();
					item3Back();
			    }
				else if (target1.transform.position == item3.transform.position)
				{
			        actionSalah(1);
					item4Back();
					item2Back();
			    }
	        }
			else 
			{
				dragObject.transform.position = startpos;
	        }
	    }
		else if (!item1target1 && !item4target1)
		{
	        if(Dis1<2)
			{
	            dragObject.transform.position = target1.transform.position;

				if (target1.transform.position == item1.transform.position)
				{
			        actionBenar(1);
					item2Back();
					item3Back();
					item4Back();
					item1target1 = true;
					target1.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
			    }
				else if (target1.transform.position == item4.transform.position)
				{
			        actionBenar(1);
					item1Back();
					item2Back();
					item3Back();
					item4target1 = true;
					target1.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
			    }
				else if (target1.transform.position == item2.transform.position)
				{
			        actionSalah(1);
					item1Back();
					item3Back();
					item4Back();
			    } 
				else if (target1.transform.position == item3.transform.position)
				{
			        actionSalah(1);
					item1Back();
					item2Back();
					item4Back();
			    }
				
	        }
			else 
			{
				dragObject.transform.position = startpos;
	        }
	    }
		
		if (item1target1 && !item4target2)
		{
	        if(Dis2<2)
			{
	            dragObject.transform.position = target2.transform.position;

				if (target2.transform.position == item4.transform.position)
				{
			        actionBenar(2);
					item2Back();
					item3Back();
					item4target2 = true;
					target2.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
			    }
				else if (target2.transform.position == item2.transform.position)
				{
			        actionSalah(2);
					item1Back();
					item3Back();
			    } 
				else if (target2.transform.position == item3.transform.position)
				{
			        actionSalah(2);
					item1Back();
					item2Back();
			    }
	        }
			else 
			{
				dragObject.transform.position = startpos;
	        }
	    }
		else if (item4target1 && !item1target2)
		{
	        if(Dis2<2)
			{
	            dragObject.transform.position = target2.transform.position;

				if (target2.transform.position == item1.transform.position)
				{
					actionBenar(2);
					item2Back();
					item3Back();
					item1target2 = true;
					target2.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
			    }
				else if (target2.transform.position == item2.transform.position)
				{
			        actionSalah(2);
					item4Back();
					item3Back();
			    }
				else if (target2.transform.position == item3.transform.position)
				{
			        actionSalah(2);
					item4Back();
					item2Back();
			    }
	        }
			else 
			{
				dragObject.transform.position = startpos;
	        }
	    }
		else if (!item1target2 && !item4target2)
		{
	        if(Dis2<2)
			{
	            dragObject.transform.position = target2.transform.position;

				if (target2.transform.position == item1.transform.position)
				{
			        actionBenar(2);
					item2Back();
					item3Back();
					item4Back();
					item1target2 = true;
					target2.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
			    }
				else if (target2.transform.position == item4.transform.position)
				{
			        actionBenar(2);
					item1Back();
					item2Back();
					item3Back();
					item4target2 = true;
					target2.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
			    }
				else if (target2.transform.position == item2.transform.position)
				{
			        actionSalah(2);
					item1Back();
					item3Back();
					item4Back();
			    } 
				else if (target2.transform.position == item3.transform.position)
				{
			        actionSalah(2);
					item1Back();
					item2Back();
					item4Back();
			    }
				
	        }
			else 
			{
				dragObject.transform.position = startpos;
	        }
	    }
		
		
        // if (sukses1 == false && sukses2 == false)
		// {
	    //     if(Dis1<2)
		// 	{
	    //         dragObject.transform.position = target1.transform.position;
				
		// 		if (target1.transform.position == item1.transform.position)
		// 		{
		// 	        actionBenar(1);
		// 			item2Back();
		// 			item3Back();
		// 			item4Back();
		// 			sukses1 = true;
		// 			target1.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
		// 	    }
		// 		else if (target1.transform.position == item4.transform.position)
		// 		{
		// 	        actionBenar(1);
		// 			item2Back();
		// 			item3Back();
		// 			item1Back();
		// 			sukses1 = true;
		// 			target1.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
		// 	    }
		// 		else if (target1.transform.position == item2.transform.position)
		// 		{
		// 	        actionSalah(1);
		// 			item1Back();
		// 			item3Back();
		// 			item4Back();
		// 	    } 
		// 		else if (target1.transform.position == item3.transform.position)
		// 		{
		// 	        actionSalah(1);
		// 			item1Back();
		// 			item2Back();
		// 			item4Back();
		// 	    }
	    //     }
		// 	else if(Dis2<2)
		// 	{
	    //         dragObject.transform.position = target2.transform.position;
		// 		if (target2.transform.position == item1.transform.position)
		// 		{
		// 	        actionBenar(2);
		// 			item2Back();
		// 			item3Back();
		// 			item4Back();
		// 			sukses2 = true;
		// 			target2.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
		// 	    }
		// 		else if (target2.transform.position == item4.transform.position)
		// 		{
		// 	        actionBenar(2);
		// 			item2Back();
		// 			item3Back();
		// 			item1Back();
		// 			sukses2 = true;
		// 			target2.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
		// 	    }
		// 		else if (target2.transform.position == item2.transform.position)
		// 		{
		// 	        actionSalah(2);
		// 			item1Back();
		// 			item3Back();
		// 			item4Back();
		// 	    } 
		// 		else if (target2.transform.position == item3.transform.position)
		// 		{
		// 	        actionSalah(2);
		// 			item1Back();
		// 			item2Back();
		// 			item4Back();
		// 	    }
				
	    //     }
		// 	else 
		// 	{
		// 		dragObject.transform.position = startpos;
	    //     }
	    // }
		// else if (!sukses1)
		// {
		// 	if(Dis1<2)
		// 	{
	    //         dragObject.transform.position = target1.transform.position;
				
		// 		if (target1.transform.position == item1.transform.position)
		// 		{
		// 	        actionBenar(1);
		// 			item2Back();
		// 			item3Back();
		// 			sukses1 = true;
		// 			target1.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
		// 	    }
		// 		else if (target1.transform.position == item4.transform.position)
		// 		{
		// 	        actionBenar(1);
		// 			item2Back();
		// 			item3Back();
		// 			item1Back();
		// 			sukses1 = true;
		// 			target1.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
		// 	    }
		// 		else if (target1.transform.position == item2.transform.position)
		// 		{
		// 	        actionSalah(1);
		// 			item1Back();
		// 			item3Back();
		// 			item4Back();
		// 	    } 
		// 		else if (target1.transform.position == item3.transform.position)
		// 		{
		// 	        actionSalah(1);
		// 			item1Back();
		// 			item2Back();
		// 			item4Back();
		// 	    }
	    //     }
		// 	else 
		// 	{
		// 		dragObject.transform.position = startpos;
	    //     }
		// }
		// else if (!sukses2)
		// {
		// 	if(Dis2<2)
		// 	{
	    //         dragObject.transform.position = target2.transform.position;
				
		// 		if (target2.transform.position == item1.transform.position)
		// 		{
		// 	        actionBenar(2);
		// 			item2Back();
		// 			item3Back();
		// 			item4Back();
		// 			sukses2 = true;
		// 			target2.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
		// 	    }
		// 		else if (target2.transform.position == item4.transform.position)
		// 		{
		// 	        actionBenar(2);
		// 			item2Back();
		// 			item3Back();
		// 			item1Back();
		// 			sukses2 = true;
		// 			target2.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
		// 	    }
		// 		else if (target2.transform.position == item2.transform.position)
		// 		{
		// 	        actionSalah(2);
		// 			item1Back();
		// 			item3Back();
		// 			item4Back();
		// 	    } 
		// 		else if (target2.transform.position == item3.transform.position)
		// 		{
		// 	        actionSalah(2);
		// 			item1Back();
		// 			item2Back();
		// 			item4Back();
		// 	    }
	    //     }
		// 	else 
		// 	{
		// 		dragObject.transform.position = startpos;
	    //     }
		// }
    }
	void actionBenar(int i)
	{
		switch(i)
		{
			case 1:
				benar1.gameObject.SetActive(true);
				salah1.gameObject.SetActive(false);
			break;
			case 2:
				benar2.gameObject.SetActive(true);
				salah2.gameObject.SetActive(false);
			break;
		}
		
	}

	void actionSalah(int i)
	{
		switch(i)
		{
			case 1:
				benar1.gameObject.SetActive(false);
				salah1.gameObject.SetActive(true);
			break;
			case 2:
				benar2.gameObject.SetActive(false);
				salah2.gameObject.SetActive(true);
			break;
		}
	}
	

	void item1Back()
	{
		item1.transform.position = startpos1;
	}

	void item2Back()
	{
		item2.transform.position = startpos2;
	}

	void item3Back()
	{
		item3.transform.position = startpos3;
	}

    void item4Back()
	{
		item4.transform.position = startpos4;
	}

	void allBack()
	{
		item1.transform.position = startpos1;
		item2.transform.position = startpos2;
		item3.transform.position = startpos3;
        item4.transform.position = startpos4;
	}
}