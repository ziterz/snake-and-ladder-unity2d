using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
 
public class DragHandler2 : MonoBehaviour, IDragHandler, IEndDragHandler 
{
    public GameObject item1, item2, item3, dragObject, target1, benar, salah, lanjutkan;
	bool sukses;
    Vector3 startpos, startpos1, startpos2, startpos3;

    void Start()
	{
		sukses = false;
		startpos = dragObject.transform.position;
        startpos1 = item1.transform.position;
		startpos2 = item2.transform.position;
		startpos3 = item3.transform.position;
    }

    public void OnDrag(PointerEventData eventData)
	{
   		if (!sukses)
		{
	        Vector3 newpos = Input.mousePosition - new Vector3 (Screen.width / 2f, Screen.height / 2f, 0f);
	        dragObject.transform.localPosition = newpos;
		}
    }
 
    public void OnEndDrag(PointerEventData eventData)
	{
        float Dis1 = Vector3.Distance(dragObject.transform.position, target1.transform.position);

		if (!sukses)
		{
	        if(Dis1<2)
			{
				allBack();
	            dragObject.transform.position = target1.transform.position;
				if (target1.transform.position == item3.transform.position)
				{
			        actionBenar();
					item1Back();
					item2Back();
					sukses = true;
					lanjutkan.SetActive(true);
					target1.transform.position = new Vector3 (Screen.width / 2f, Screen.height / 2f, 99);
			    }
				else if (target1.transform.position == item1.transform.position)
				{
			        actionSalah();
					item2Back();
					item3Back();
			    } 
				else if (target1.transform.position == item2.transform.position)
				{
			        actionSalah();
					item1Back();
					item3Back();
			    }
				
	        }
			else 
			{
				dragObject.transform.position = startpos;
	        }
	    }
    }
	void actionBenar()
	{
		benar.gameObject.SetActive(true);
		salah.gameObject.SetActive(false);
	}

	void actionSalah()
	{
		benar.gameObject.SetActive(false);
		salah.gameObject.SetActive(true);
	}
	
	void item1Back()
	{
		item1.transform.position = startpos1;
	}

	void item2Back()
	{
		item2.transform.position = startpos2;
	}

	void item3Back()
	{
		item3.transform.position = startpos3;
	}

	void allBack()
	{
		item1.transform.position = startpos1;
		item2.transform.position = startpos2;
		item3.transform.position = startpos3;
	}
}