using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
 
 //Sub2Game1
public class DragSub4Game1 : MonoBehaviour, IDragHandler, IEndDragHandler 
{
    Vector3 mouseStartPos, playerStartPos;

    public GameObject item1, item2, dragObject, target1, target2, target3, target4, target5, target6, target7, target8, target9, target10, benar, salah, lanjutkan;
    Vector3 startpos, startpos1, startpos2;

    void Start()
	{
        startpos = dragObject.transform.position;
        startpos1 = item1.transform.position;
		startpos2 = item2.transform.position;
    }

    public void OnDrag(PointerEventData eventData)
	{
   		Vector3 newpos = Input.mousePosition - new Vector3 (Screen.width / 2f, Screen.height / 2f, 0f);
	    dragObject.transform.localPosition = newpos;
    }
 
    public void OnEndDrag(PointerEventData eventData)
	{
        float Dis1 = Vector3.Distance(dragObject.transform.position, target1.transform.position);
        float Dis2 = Vector3.Distance(dragObject.transform.position, target2.transform.position);
        float Dis3 = Vector3.Distance(dragObject.transform.position, target3.transform.position);
        float Dis4 = Vector3.Distance(dragObject.transform.position, target4.transform.position);
        float Dis5 = Vector3.Distance(dragObject.transform.position, target5.transform.position);
        float Dis6 = Vector3.Distance(dragObject.transform.position, target6.transform.position);
        float Dis7 = Vector3.Distance(dragObject.transform.position, target7.transform.position);
        float Dis8 = Vector3.Distance(dragObject.transform.position, target8.transform.position);
        float Dis9 = Vector3.Distance(dragObject.transform.position, target9.transform.position);
        float Dis10 = Vector3.Distance(dragObject.transform.position, target10.transform.position);

		if(Dis1<2)
        {
            if (item1.transform.position == target1.transform.position) {
                item1Back();
            } 
            else if (item2.transform.position == target1.transform.position) {
                item2Back();
            }
            dragObject.transform.position = target1.transform.position;
        }
        else if(Dis2<2)
        {
            if (item1.transform.position == target2.transform.position) {
                item1Back();
            } 
            else if (item2.transform.position == target2.transform.position) {
                item2Back();
            }
            dragObject.transform.position = target2.transform.position;
        }
        else if(Dis3<2)
        {
            if (item1.transform.position == target3.transform.position) {
                item1Back();
            } 
            else if (item2.transform.position == target3.transform.position) {
                item2Back();
            }
            dragObject.transform.position = target3.transform.position;
        }
        else if(Dis4<2)
        {
            if (item1.transform.position == target4.transform.position) {
                item1Back();
            } 
            else if (item2.transform.position == target4.transform.position) {
                item2Back();
            }
            dragObject.transform.position = target4.transform.position;
        }
        else if(Dis5<2)
        {
            if (item1.transform.position == target5.transform.position) {
                item1Back();
            } 
            else if (item2.transform.position == target5.transform.position) {
                item2Back();
            }
            dragObject.transform.position = target5.transform.position;
        }
        else if(Dis6<2)
        {
            if (item1.transform.position == target6.transform.position) {
                item1Back();
            } 
            else if (item2.transform.position == target6.transform.position) {
                item2Back();
            }
            dragObject.transform.position = target6.transform.position;
        }
        else if(Dis7<2)
        {
            if (item1.transform.position == target7.transform.position) {
                item1Back();
            } 
            else if (item2.transform.position == target7.transform.position) {
                item2Back();
            }
            dragObject.transform.position = target7.transform.position;
        }
        else if(Dis8<2)
        {
            if (item1.transform.position == target8.transform.position) {
                item1Back();
            } 
            else if (item2.transform.position == target8.transform.position) {
                item2Back();
            }
            dragObject.transform.position = target8.transform.position;
        }
        else if(Dis9<2)
        {
            if (item1.transform.position == target9.transform.position) {
                item1Back();
            } 
            else if (item2.transform.position == target9.transform.position) {
                item2Back();
            }
            dragObject.transform.position = target9.transform.position;
        }
        else if(Dis10<2)
        {
            if (item1.transform.position == target10.transform.position) {
                item1Back();
            } 
            else if (item2.transform.position == target10.transform.position) {
                item2Back();
            }
            dragObject.transform.position = target10.transform.position;
        }
        else 
        {
            dragObject.transform.position = startpos;
        }
    }
	
	void item1Back()
	{
		item1.transform.position = startpos1;
	}

	void item2Back()
	{
		item2.transform.position = startpos2;
	}
}