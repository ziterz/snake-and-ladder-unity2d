using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AddPosttest : MonoBehaviour
{

    public TMP_Text nilai;
    public void OnSubmit()
    {
        PlayerPrefs.SetInt("Posstest", PlayerPrefs.GetInt("Posstest") + 5);
    }

    public void OnFinish()
    {
        nilai.text = PlayerPrefs.GetInt("Posstest").ToString();
    }
}
