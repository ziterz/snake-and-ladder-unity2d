using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TypingScript2 : MonoBehaviour
{
    // Start is called before the first frame update
    public float delay = 0.1f;

    public GameObject next;
    public TMP_Text text1;
    
    public string fullText;
    private string currentText = "";
    public void OnClick()
    {
        StartCoroutine(ShowText());
    }

    // Update is called once per frame
    IEnumerator ShowText(){
    	for(int i = 0; i <= fullText.Length; i++) {
            if(i == fullText.Length) {
                next.SetActive(true);
            }
    		currentText = fullText.Substring(0,i);
    		text1.text = currentText;
    		yield return new WaitForSeconds(delay);
    	}
    }
}
