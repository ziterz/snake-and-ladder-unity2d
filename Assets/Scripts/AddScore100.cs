﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddScore100 : MonoBehaviour
{
    public void OnSubmit()
    {
        switch(PlayerPrefs.GetInt("Level"))
        {
            case 1:
                PlayerPrefs.SetInt("Chapter1", PlayerPrefs.GetInt("Chapter1") + 100);
            break;

            case 2:
                PlayerPrefs.SetInt("Chapter1", PlayerPrefs.GetInt("Chapter1") + 100);
            break;

            case 3:
                PlayerPrefs.SetInt("Chapter1", PlayerPrefs.GetInt("Chapter1") + 100);
            break;

            case 4:
                PlayerPrefs.SetInt("Chapter1", PlayerPrefs.GetInt("Chapter1") + 100);
            break;

            case 5:
                PlayerPrefs.SetInt("Chapter1", PlayerPrefs.GetInt("Chapter1") + 100);
            break;

            case 6:
                PlayerPrefs.SetInt("Chapter1", PlayerPrefs.GetInt("Chapter1") + 100);
            break;
            
            case 7:
                PlayerPrefs.SetInt("Chapter1", PlayerPrefs.GetInt("Chapter1") + 100);
            break;

            case 8:
                PlayerPrefs.SetInt("Chapter1", PlayerPrefs.GetInt("Chapter1") + 100);
            break;

            case 9:
                PlayerPrefs.SetInt("Chapter1", PlayerPrefs.GetInt("Chapter1") + 100);
            break;

            case 10:
                PlayerPrefs.SetInt("Chapter2", PlayerPrefs.GetInt("Chapter2") + 100);
            break;

            case 11:
                PlayerPrefs.SetInt("Chapter2", PlayerPrefs.GetInt("Chapter2") + 100);
            break;

            case 12:
                PlayerPrefs.SetInt("Chapter2", PlayerPrefs.GetInt("Chapter2") + 100);
            break;

            case 13:
                PlayerPrefs.SetInt("Chapter2", PlayerPrefs.GetInt("Chapter2") + 100);
            break;

            case 14:
                PlayerPrefs.SetInt("Chapter2", PlayerPrefs.GetInt("Chapter2") + 100);
            break;

            case 15:
                PlayerPrefs.SetInt("Chapter2", PlayerPrefs.GetInt("Chapter2") + 100);
            break;

            case 16:
                PlayerPrefs.SetInt("Chapter2", PlayerPrefs.GetInt("Chapter2") + 100);
            break;

            case 18:
                PlayerPrefs.SetInt("Chapter3", PlayerPrefs.GetInt("Chapter3") + 100);
            break;

            case 19:
                PlayerPrefs.SetInt("Chapter3", PlayerPrefs.GetInt("Chapter3") + 100);
            break;

            case 20:
                PlayerPrefs.SetInt("Chapter3", PlayerPrefs.GetInt("Chapter3") + 100);
            break;

            case 21:
                PlayerPrefs.SetInt("Chapter3", PlayerPrefs.GetInt("Chapter3") + 100);
            break;

            case 22:
                PlayerPrefs.SetInt("Chapter3", PlayerPrefs.GetInt("Chapter3") + 100);
            break;

            case 24:
                PlayerPrefs.SetInt("Chapter4", PlayerPrefs.GetInt("Chapter4") + 100);
            break;

            case 25:
                PlayerPrefs.SetInt("Chapter4", PlayerPrefs.GetInt("Chapter4") + 100);
            break;

            case 26:
                PlayerPrefs.SetInt("Chapter4", PlayerPrefs.GetInt("Chapter4") + 100);
            break;

            case 27:
                PlayerPrefs.SetInt("Chapter4", PlayerPrefs.GetInt("Chapter4") + 100);
            break;

            case 28:
                PlayerPrefs.SetInt("Chapter4", PlayerPrefs.GetInt("Chapter4") + 100);
            break;

            case 29:
                PlayerPrefs.SetInt("Chapter4", PlayerPrefs.GetInt("Chapter4") + 100);
            break;

            case 31:
                PlayerPrefs.SetInt("Chapter5", PlayerPrefs.GetInt("Chapter5") + 100);
            break;

            case 32:
                PlayerPrefs.SetInt("Chapter5", PlayerPrefs.GetInt("Chapter5") + 100);
            break;

            case 33:
                PlayerPrefs.SetInt("Chapter5", PlayerPrefs.GetInt("Chapter5") + 100);
            break;

            case 34:
                PlayerPrefs.SetInt("Chapter5", PlayerPrefs.GetInt("Chapter5") + 100);
            break;

            case 35:
                PlayerPrefs.SetInt("Chapter5", PlayerPrefs.GetInt("Chapter5") + 100);
            break;

            case 37:
                PlayerPrefs.SetInt("Chapter6", PlayerPrefs.GetInt("Chapter6") + 100);
            break;

            case 38:
                PlayerPrefs.SetInt("Chapter6", PlayerPrefs.GetInt("Chapter6") + 100);
            break;

            case 39:
                PlayerPrefs.SetInt("Chapter6", PlayerPrefs.GetInt("Chapter6") + 100);
            break;

            case 41:
                PlayerPrefs.SetInt("Chapter7", PlayerPrefs.GetInt("Chapter7") + 100);
            break;

            case 42:
                PlayerPrefs.SetInt("Chapter7", PlayerPrefs.GetInt("Chapter7") + 100);
            break;

            default:
                PlayerPrefs.SetInt("AllChapter", PlayerPrefs.GetInt("AllChapter") + 100);
            break;
        }
    }
}
