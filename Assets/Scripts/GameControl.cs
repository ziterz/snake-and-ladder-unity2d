﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using TMPro;

public class GameControl : MonoBehaviour
{
    public static GameObject moveText, player, mainPanel, helpPanel, winnerPanel, board, help, setting, resume, pause;

    public TMP_Text winnerName, score1, score2, score3, score4, score5, score6, score7, score8, winnerScore;

    public int chapter1, chapter2, chapter3, chapter4, chapter5, chapter6, chapter7, chapter8, allchapter;

    public static int diceSideThrown;
    public static int playerStartWaypoint = 0;

    public static bool gameOver = false;
    public static bool lose = false;
    public AudioSource win_sound, music;
    void Start()
    {
        if(PlayerPrefs.GetInt("Dice") > 0)
        {
            diceSideThrown = PlayerPrefs.GetInt("Dice");
        }
        else 
        {
            diceSideThrown = 6;
        }
        
        PlayerPrefs.SetInt("Dice", diceSideThrown);
        PlayerPrefs.Save();

        help = GameObject.Find("Help");
        setting = GameObject.Find("Setting");
        resume = GameObject.Find("Resume");
        pause = GameObject.Find("Pause");


        player = GameObject.Find("Player");
        board = GameObject.Find("Board");
        moveText = GameObject.Find("MoveText");

        mainPanel = GameObject.Find("MainPanel");
        winnerPanel = GameObject.Find("WinnerPanel");
        helpPanel = GameObject.Find("HelpPanel");

        if(PlayerPrefs.GetInt("Intro") == 1)
        {
            help.gameObject.SetActive(false);
            setting.gameObject.SetActive(false);
            resume.gameObject.SetActive(false);
            pause.gameObject.SetActive(false);
            mainPanel.gameObject.SetActive(false);
            player.gameObject.SetActive(false);
            board.gameObject.SetActive(false);
            helpPanel.gameObject.SetActive(true);
            winnerPanel.gameObject.SetActive(false);
            PlayerPrefs.SetInt("Intro", 0);
        }
        else
        {
            help.gameObject.SetActive(true);
            setting.gameObject.SetActive(true);
            resume.gameObject.SetActive(true);
            pause.gameObject.SetActive(true);
            mainPanel.gameObject.SetActive(true);
            player.gameObject.SetActive(true);
            board.gameObject.SetActive(true);
            helpPanel.gameObject.SetActive(false);
            winnerPanel.gameObject.SetActive(false);
        }

        moveText.gameObject.SetActive(true);

        player.GetComponent<FollowThePath>().moveAllowed = false;

        chapter1    = PlayerPrefs.GetInt("Chapter1");
        chapter2    = PlayerPrefs.GetInt("Chapter2");
        chapter3    = PlayerPrefs.GetInt("Chapter3");
        chapter4    = PlayerPrefs.GetInt("Chapter4");
        chapter5    = PlayerPrefs.GetInt("Chapter5");
        chapter6    = PlayerPrefs.GetInt("Chapter6");
        chapter7    = PlayerPrefs.GetInt("Chapter7");
        chapter8    = PlayerPrefs.GetInt("Chapter8");
        allchapter  = PlayerPrefs.GetInt("AllChapter");
    }

    // Update is called once per frame
    void Update()
    {
        if(gameOver)
        {

        }
        else if (player.GetComponent<FollowThePath>().waypointIndex >= 100)
        {
            gameOver = true;
            StartCoroutine(Delay());
        }
        else
        {
            // Debug.Log("playerStartWaypoint: " + playerStartWaypoint + "Waypoint: " + player.GetComponent<FollowThePath>().waypointIndex + "Waypoint: " + diceSideThrown);
            PlayerPrefs.SetInt("Dice", diceSideThrown);
            PlayerPrefs.Save();
            if (player.GetComponent<FollowThePath>().waypointIndex > playerStartWaypoint + diceSideThrown)
            {
                if (PlayerPrefs.GetInt("Status") == 1)
                {
                    PlayerPrefs.SetInt("Level",PlayerPrefs.GetInt("Level")+1);
                    PlayerPrefs.Save();
                    StartCoroutine(OpenScene());
                }
                player.GetComponent<FollowThePath>().moveAllowed = false;
                player.GetComponent<FollowThePath>().upStairAllowed = false;
                playerStartWaypoint = player.GetComponent<FollowThePath>().waypointIndex - 1;
            }
            else if (player.GetComponent<FollowThePath>().waypointIndex < playerStartWaypoint)
            {
                player.GetComponent<FollowThePath>().moveAllowed = false;
                player.GetComponent<FollowThePath>().downStairAllowed = false;
                playerStartWaypoint = player.GetComponent<FollowThePath>().waypointIndex - 1;
            }
        }
    }

    IEnumerator OpenScene()
    {
        yield return new WaitForSeconds(0.5f);
        switch(PlayerPrefs.GetInt("Level"))
        {
            case 17:
                SceneManager.LoadScene(4);
            break;

            case 23:
                SceneManager.LoadScene(4);
            break;

            case 30:
                SceneManager.LoadScene(4);
            break;

            case 43:
                SceneManager.LoadScene(4);
            break;

            default:
                SceneManager.LoadScene(3);
            break;
        }
        
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.5f);
        mainPanel.gameObject.SetActive(false);
        player.gameObject.SetActive(false);
        board.gameObject.SetActive(false);
        help.gameObject.SetActive(false);
        setting.gameObject.SetActive(false);
        resume.gameObject.SetActive(false);
        pause.gameObject.SetActive(false);
        winnerPanel.gameObject.SetActive(true);

        win_sound.Play(0);
        music.Stop();

        winnerName.text = PlayerPrefs.GetString("Username");
        score1.text = chapter1.ToString();
        score2.text = chapter2.ToString();
        score3.text = chapter3.ToString();
        score4.text = chapter4.ToString();
        score5.text = chapter5.ToString();
        score6.text = chapter6.ToString();
        score7.text = chapter7.ToString();
        score8.text = chapter8.ToString();
        winnerScore.text = (chapter1+chapter2+chapter3+chapter4+chapter5+chapter6+chapter7+chapter8+allchapter).ToString();

        moveText.gameObject.SetActive(false);
    }

    public static void MovePlayer(int playerToMove)
    {
        if (playerToMove == 1)
        {
            moveText.gameObject.SetActive(false);
            player.GetComponent<FollowThePath>().moveAllowed = true;
        }
    }
}

