using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
 
public class Challenge1Part1 : MonoBehaviour
{
    public GameObject space, benar, salah, gagal, berhasil, cek;
    public TMP_InputField field1, field2, field3;
    public AudioSource correct, incorrect;

    public void OnCek()
	{
        if(field1.text.Equals("1") && field2.text.Equals("5") && field3.text.Equals("2"))
        {
            correct.Play(0);
            actionBenar();
            cek.SetActive(false);
            berhasil.SetActive(true);
            space.GetComponent<LineChallenge>().waypointIndex += 1;
        }
        else
        {
            incorrect.Play(0);
            actionSalah();
            cek.SetActive(false);
            gagal.SetActive(true);
            PlayerPrefs.SetInt("VideoLevel", 1);
        }
    }

    void actionBenar()
	{
		benar.gameObject.SetActive(true);
		salah.gameObject.SetActive(false);
	}

	void actionSalah()
	{
		salah.gameObject.SetActive(true);
		benar.gameObject.SetActive(false);
	}
}