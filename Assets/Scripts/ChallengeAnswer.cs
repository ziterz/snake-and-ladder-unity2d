﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ChallengeAnswer : MonoBehaviour
{
    // Start is called before the first frame update
    public void OnSuccess()
    {
        PlayerPrefs.SetInt("ChallengeSuccess", 1);
        SceneManager.LoadScene(6);
    }

    public void OnFailure()
    {
        PlayerPrefs.SetInt("ChallengeSuccess", 2);
        SceneManager.LoadScene(6);
    }
}
