using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OpenLogic : MonoBehaviour
{
    public GameObject openMateri1, openMateri2, openMateri3, openMateri4, openMateri5, openMateri6, openMateri7, openMateri8;
    void Start()
    {
        initObject();
        initAction();
    }

    void initObject()
    {
        openMateri1 = GameObject.Find("OpenMateri1");
        openMateri1.gameObject.SetActive(false);

        openMateri2 = GameObject.Find("OpenMateri2");
        openMateri2.gameObject.SetActive(false);

        openMateri3 = GameObject.Find("OpenMateri3");
        openMateri3.gameObject.SetActive(false);

        openMateri4 = GameObject.Find("OpenMateri4");
        openMateri4.gameObject.SetActive(false);

        openMateri5 = GameObject.Find("OpenMateri5");
        openMateri5.gameObject.SetActive(false);

        openMateri6 = GameObject.Find("OpenMateri6");
        openMateri6.gameObject.SetActive(false);

        openMateri7 = GameObject.Find("OpenMateri7");
        openMateri7.gameObject.SetActive(false);

        openMateri8 = GameObject.Find("OpenMateri8");
        openMateri8.gameObject.SetActive(false);
    }

    void initAction()
    {
        switch(PlayerPrefs.GetInt("OpenMateri"))
        {
            case 1:
                openMateri1.gameObject.SetActive(true);
            break;

            case 2:
                openMateri2.gameObject.SetActive(true);
            break;

            case 3:
                openMateri3.gameObject.SetActive(true);
            break;

            case 4:
                openMateri4.gameObject.SetActive(true);
            break;

            case 5:
                openMateri5.gameObject.SetActive(true);
            break;

            case 6:
                openMateri6.gameObject.SetActive(true);
            break;

            case 7:
                openMateri7.gameObject.SetActive(true);
            break;

            case 8:
                openMateri8.gameObject.SetActive(true);
            break;
        }   
    }
}
