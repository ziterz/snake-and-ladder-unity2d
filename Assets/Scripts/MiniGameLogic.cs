﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniGameLogic : MonoBehaviour
{
    public GameObject blank1, blank2, blank3;
    public GameObject focus1, focus2, focus3;
    public GameObject smile1, smile2, smile3;
    public GameObject smilegreen1, smilegreen2, smilegreen3;
    public GameObject sad1, sad2, sad3;
    public GameObject sadgreen1, sadgreen2, sadgreen3;

    public GameObject mini1game1, mini1game2, mini1game3, mini1final;

    // Start is called before the first frame update
    void Start()
    {

        mini1game1 = GameObject.Find("Mini1Game1");
        mini1game2 = GameObject.Find("Mini1Game2");
        mini1game3 = GameObject.Find("Mini1Game3");
        mini1final = GameObject.Find("Mini1Final");

        blank1 = GameObject.Find("Blank1");
        blank2 = GameObject.Find("Blank2");
        blank3 = GameObject.Find("Blank3");

        focus1 = GameObject.Find("Focus1");
        focus2 = GameObject.Find("Focus2");
        focus3 = GameObject.Find("Focus3");

        smile1 = GameObject.Find("Smile1");
        smile2 = GameObject.Find("Smile2");
        smile3 = GameObject.Find("Smile3");

        smilegreen1 = GameObject.Find("SmileGreen1");
        smilegreen2 = GameObject.Find("SmileGreen2");
        smilegreen3 = GameObject.Find("SmileGreen3");

        sad1 = GameObject.Find("Sad1");
        sad2 = GameObject.Find("Sad2");
        sad3 = GameObject.Find("Sad3");

        sadgreen1 = GameObject.Find("SadGreen1");
        sadgreen2 = GameObject.Find("SadGreen2");
        sadgreen3 = GameObject.Find("SadGreen3");

    }
}
