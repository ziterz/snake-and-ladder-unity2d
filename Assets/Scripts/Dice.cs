﻿using System.Collections;
using UnityEngine;

public class Dice : MonoBehaviour {

    private Sprite[] diceSides;
    private SpriteRenderer rend;
    public bool coroutineAllowed = true;

    public AudioSource rolldice;

    private static GameObject dice;

    // Use this for initialization
    private void Start () {

        dice = GameObject.Find("Dice");

        rend = GetComponent<SpriteRenderer>();
        diceSides = Resources.LoadAll<Sprite>("DiceSides/");
        // Debug.Log("Dice value:" + PlayerPrefs.GetInt("Dice"));
        if(PlayerPrefs.GetInt("Dice") > 0 && PlayerPrefs.GetInt("Dice") <= 6)
        {
            rend.sprite = diceSides[PlayerPrefs.GetInt("Dice")-1];
        }
        else
        {
            rend.sprite = diceSides[5];
        }
        
	}

    private void OnMouseDown()
    {
        if (!GameControl.gameOver && coroutineAllowed)
        {
            rolldice.Play(0);
            coroutineAllowed = false;
            GameControl.moveText.gameObject.SetActive(false);
            StartCoroutine("RollTheDice");
        }     
    }

    private IEnumerator RollTheDice()
    {
        int randomDiceSide = 0;
        if (PlayerPrefs.GetInt("Position") >= 0 && PlayerPrefs.GetInt("Position") < 5 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(0, 2);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 5 && PlayerPrefs.GetInt("Position") < 8 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(0, 1);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 8 && PlayerPrefs.GetInt("Position") < 10 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(0, 1);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 10 && PlayerPrefs.GetInt("Position") < 15 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(0, 2);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 15 && PlayerPrefs.GetInt("Position") < 20 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(0, 1);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 20 && PlayerPrefs.GetInt("Position") < 30 )
        {
            for (int i = 0; i <= 20; i++)
            {
                randomDiceSide = Random.Range(0, 3);
                rend.sprite = diceSides[randomDiceSide];
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 30 && PlayerPrefs.GetInt("Position") < 50 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(1, 2);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 50 && PlayerPrefs.GetInt("Position") < 55 )
        {
            for (int i = 0; i <= 20; i++)
            {
                randomDiceSide = Random.Range(5, 6);
                rend.sprite = diceSides[randomDiceSide];
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 55 && PlayerPrefs.GetInt("Position") < 60)
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(0, 2);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 60 && PlayerPrefs.GetInt("Position") < 65 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(5, 5);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 65 && PlayerPrefs.GetInt("Position") < 75 )
        {
            for (int i = 0; i <= 20; i++)
            {
                randomDiceSide = Random.Range(0, 2);
                rend.sprite = diceSides[randomDiceSide];
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 75 && PlayerPrefs.GetInt("Position") < 80 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(0, 1);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 80 && PlayerPrefs.GetInt("Position") < 85 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(0, 2);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 85 && PlayerPrefs.GetInt("Position") < 90 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(0, 3);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") >= 85 && PlayerPrefs.GetInt("Position") <= 93 )
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = Random.Range(0, 4);
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") == 94)
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = 5;
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") == 95)
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = 4;
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") == 96)
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = 3;
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") == 97)
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = 2;
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") == 98)
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = 1;
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else if (PlayerPrefs.GetInt("Position") == 99)
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i >= 15)
                {
                    randomDiceSide = 0;
                    rend.sprite = diceSides[randomDiceSide];
                }
                else
                {
                    randomDiceSide = Random.Range(0, 6);
                    rend.sprite = diceSides[randomDiceSide];
                }
                
                yield return new WaitForSeconds(0.05f);
            }
        }
        else
        {
            for (int i = 0; i <= 20; i++)
            {
                randomDiceSide = Random.Range(0, 6);
                rend.sprite = diceSides[randomDiceSide];
                yield return new WaitForSeconds(0.05f);
            }
        }
        GameControl.diceSideThrown = randomDiceSide + 1;
        GameControl.MovePlayer(1);
    }
    
}
