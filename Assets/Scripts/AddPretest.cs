using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AddPretest : MonoBehaviour
{

    public TMP_Text nilai;
    public void OnSubmit()
    {
        PlayerPrefs.SetInt("Pretest", PlayerPrefs.GetInt("Pretest") + 5);
    }

    public void OnFinish()
    {
        nilai.text = PlayerPrefs.GetInt("Pretest").ToString();
    }
}
