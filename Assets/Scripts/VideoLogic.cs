using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VideoLogic : MonoBehaviour
{
    //Sub Materi 1
    public GameObject sub1video1;
    //Sub Materi 2
    public GameObject sub2video1;
    //Sub Materi 3
    public GameObject sub3video1;
    //Sub Materi 4
    public GameObject sub4video1;
    //Sub Materi 5
    public GameObject sub5video1;
    //Sub Materi 6
    public GameObject sub6video1;
    //Sub Materi 7
    public GameObject sub7video1;
    //Sub Materi 8
    public GameObject sub8video1;

    public int chapter1, chapter2, chapter3, chapter4, chapter5, chapter6, chapter7, chapter8, allchapter;
    void Start()
    {
        initObject();
        initAction();
    }

    public void Change(int Status)
    {
        PlayerPrefs.SetInt("Status", Status);
    }

    void initObject()
    {
        //Sub Materi 1
        sub1video1 = GameObject.Find("Sub1Video1");
        sub1video1.gameObject.SetActive(false);

        //Sub Materi 2
        sub2video1  = GameObject.Find("Sub2Video1");
        sub2video1.gameObject.SetActive(false);

        //Sub Materi 3
        sub3video1  = GameObject.Find("Sub3Video1");
        sub3video1.gameObject.SetActive(false);

        //Sub Materi 4
        sub4video1  = GameObject.Find("Sub4Video1");
        sub4video1.gameObject.SetActive(false);

        //Sub Materi 5
        sub5video1  = GameObject.Find("Sub5Video1");
        sub5video1.gameObject.SetActive(false);

        //Sub Materi 6
        sub6video1  = GameObject.Find("Sub6Video1");
        sub6video1.gameObject.SetActive(false);

        //Sub Materi 7
        sub7video1  = GameObject.Find("Sub7Video1");
        sub7video1.gameObject.SetActive(false);

        //Sub Materi 8
        sub8video1  = GameObject.Find("Sub8Video1");
        sub8video1.gameObject.SetActive(false);
    }

    void initAction()
    {
        Debug.Log("action" + PlayerPrefs.GetInt("VideoLevel"));
        switch(PlayerPrefs.GetInt("VideoLevel"))
        {
            case 1:
                sub1video1.gameObject.SetActive(true);
            break;

            case 2:
                sub2video1.gameObject.SetActive(true);
            break;

            case 3:
                sub3video1.gameObject.SetActive(true);
            break;

            case 4:
                sub4video1.gameObject.SetActive(true);
            break;

            case 5:
                sub5video1.gameObject.SetActive(true);
            break;

            case 6:
                sub6video1.gameObject.SetActive(true);
            break;

            case 7:
                sub7video1.gameObject.SetActive(true);
            break;

            case 8:
                sub8video1.gameObject.SetActive(true);
            break;
        }   
    }
}
