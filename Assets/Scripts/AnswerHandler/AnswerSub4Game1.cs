using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
 
 //Sub2Game1
public class AnswerSub4Game1 : MonoBehaviour
{
    public GameObject benar, salah, lanjutkan, cek, garis;
    public AudioSource correct, incorrect;
    public GameObject item1, item2, target1, target2, target3, target4, target5, target6, target7, target8, target9, target10;

    public void OnCek()
	{
        if(item1.transform.position == target4.transform.position && item2.transform.position == target6.transform.position)
        {
            correct.Play(0);
            actionBenar();
            cek.SetActive(false);
            lanjutkan.SetActive(true);
            garis.SetActive(true);
        }
        else
        {
            incorrect.Play(0);
            actionSalah();
        }
    }

    void actionBenar()
	{
		benar.gameObject.SetActive(true);
		salah.gameObject.SetActive(false);
	}

	void actionSalah()
	{
		salah.gameObject.SetActive(true);
		benar.gameObject.SetActive(false);
	}
}