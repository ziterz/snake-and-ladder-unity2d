using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
 
public class AnswerSub4Soal33 : MonoBehaviour
{
    public GameObject benar, salah, lanjutkan, item1, item2, item3;
    public AudioSource correct, incorrect;

    public void OnCek(int i)
	{
        if(i == 1)
        {
            correct.Play(0);
            actionBenar();
            lanjutkan.SetActive(true);
        }
        else
        {
            incorrect.Play(0);
            actionSalah();
        }
    }

    void actionBenar()
	{
		benar.gameObject.SetActive(true);
		salah.gameObject.SetActive(false);
	}

	void actionSalah()
	{
		salah.gameObject.SetActive(true);
		benar.gameObject.SetActive(false);
	}
}