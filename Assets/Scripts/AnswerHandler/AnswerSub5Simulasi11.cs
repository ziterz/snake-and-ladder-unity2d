using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
 
public class AnswerSub5Simulasi11 : MonoBehaviour
{
    public GameObject benar, salah, lanjutkan, cek;
    public TMP_InputField field1, field2, field3, field4, field5, field6, field7;

    public AudioSource correct, incorrect;

    public void OnCek()
	{
        if(field1.text.Equals("4") && field2.text.Equals("y") && field3.text.Equals("8") && field4.text.Equals("2") && field5.text.Equals("6") && field6.text.Equals("8") && field7.text.Equals("2"))
        {
            correct.Play(0);
            actionBenar();
            cek.SetActive(false);
            lanjutkan.SetActive(true);
        }
        else
        {
            incorrect.Play(0);
            actionSalah();
        }
    }

    void actionBenar()
	{
		benar.gameObject.SetActive(true);
		salah.gameObject.SetActive(false);
	}

	void actionSalah()
	{
		salah.gameObject.SetActive(true);
		benar.gameObject.SetActive(false);
	}
}