using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
 
public class AnswerSub6Simulasi11 : MonoBehaviour
{
    public GameObject benar, salah, lanjutkan, cek;
    public TMP_InputField field1, field2, field3, field4, field5, field6, field7, field8, field9, field10, field11, field12, field13, field14, field15;

    public AudioSource correct, incorrect;

    public void OnCek()
	{
        if(field1.text.Equals("4") && field2.text.Equals("-1") && field3.text.Equals("6") && field4.text.Equals("3") && field5.text.Equals("3") && field6.text.Equals("-2") && field7.text.Equals("-2") && field8.text.Equals("1") && field9.text.Equals("-3") && field10.text.Equals("2") && field11.text.Equals("2") && field12.text.Equals("1") && field13.text.Equals("2") && field14.text.Equals("3") && field15.text.Equals("4"))
        {
            correct.Play(0);
            actionBenar();
            cek.SetActive(false);
            lanjutkan.SetActive(true);
        }
        else
        {
            incorrect.Play(0);
            actionSalah();
        }
    }

    void actionBenar()
	{
		benar.gameObject.SetActive(true);
		salah.gameObject.SetActive(false);
	}

	void actionSalah()
	{
		salah.gameObject.SetActive(true);
		benar.gameObject.SetActive(false);
	}
}