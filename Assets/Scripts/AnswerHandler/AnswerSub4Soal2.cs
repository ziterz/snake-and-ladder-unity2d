using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
 
public class AnswerSub4Soal2 : MonoBehaviour
{
    public GameObject benar, salah, lanjutkan, cek, garis;
    public TMP_InputField field1, field2;
    public AudioSource correct, incorrect;

    public void OnCek()
	{
        Debug.Log(field1.text + " " + field2.text);
        if(field1.text.Equals("2") && field2.text.Equals("1"))
        {
            correct.Play(0);
            actionBenar();
            cek.SetActive(false);
            garis.SetActive(true);
            lanjutkan.SetActive(true);
        }
        else
        {
            incorrect.Play(0);
            actionSalah();
        }
    }

    void actionBenar()
	{
		benar.gameObject.SetActive(true);
		salah.gameObject.SetActive(false);
	}

	void actionSalah()
	{
		salah.gameObject.SetActive(true);
		benar.gameObject.SetActive(false);
	}
}