﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerData : MonoBehaviour
{
    public TMP_Text usernameText, scoreText;
    public string username;
    public int position, level, sublevel, bonus, chapter1, chapter2, chapter3, chapter4, chapter5, chapter6, chapter7, chapter8, allchapter;
    public void Start()
    {
        username    = PlayerPrefs.GetString("Username");
        level       = PlayerPrefs.GetInt("Level");
        position    = PlayerPrefs.GetInt("Position");
        chapter1    = PlayerPrefs.GetInt("Chapter1");
        chapter2    = PlayerPrefs.GetInt("Chapter2");
        chapter3    = PlayerPrefs.GetInt("Chapter3");
        chapter4    = PlayerPrefs.GetInt("Chapter4");
        chapter5    = PlayerPrefs.GetInt("Chapter5");
        chapter6    = PlayerPrefs.GetInt("Chapter6");
        chapter7    = PlayerPrefs.GetInt("Chapter7");
        chapter8    = PlayerPrefs.GetInt("Chapter8");
        allchapter  = PlayerPrefs.GetInt("AllChapter");
    
        usernameText.text = PlayerPrefs.GetString("Username");
        scoreText.text = (chapter1+chapter2+chapter3+chapter4+chapter5+chapter6+chapter7+chapter8+allchapter).ToString();
    }
}
