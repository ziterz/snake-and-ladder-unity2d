﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

[RequireComponent (typeof(AudioSource))]

public class HideVideo1 : MonoBehaviour
{
    //Video 1
    private VideoPlayer videoPlayer;
    private GameObject video, button, bgvideo;

    void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer> ();
        video = GameObject.Find("Video1");
        bgvideo = GameObject.Find("BgVideo1");
        button = GameObject.Find("VideoButton1");
    }
    // Start is called before the first frame update
    void Start()
    {
        
        button.gameObject.SetActive(false);

        videoPlayer.targetTexture.Release();
        videoPlayer.loopPointReached += CheckOver;
    }

    void CheckOver(UnityEngine.Video.VideoPlayer vp)
    {
        videoPlayer.Stop();
        video.gameObject.SetActive(false);
        bgvideo.gameObject.SetActive(false);
        button.gameObject.SetActive(true);
    }
}
