using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

[RequireComponent (typeof(AudioSource))]

public class HideVideo4 : MonoBehaviour
{
    //Video 1
    private VideoPlayer videoPlayer;
    private GameObject video, button, bgvideo;

    void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer> ();
        video = GameObject.Find("Video4");
        bgvideo = GameObject.Find("BgVideo4");
        button = GameObject.Find("VideoButton4");
    }
    // Start is called before the first frame update
    void Start()
    {
        button.gameObject.SetActive(false);

        videoPlayer.targetTexture.Release();
        videoPlayer.loopPointReached += CheckOver;
    }

    void CheckOver(UnityEngine.Video.VideoPlayer vp)
    {
        videoPlayer.Stop();
        video.gameObject.SetActive(false);
        bgvideo.gameObject.SetActive(false);
        button.gameObject.SetActive(true);
    }
}
