using UnityEngine;
using System.Collections;

public class LineChallenge : MonoBehaviour 
{
    public Transform[] waypoints;

    [SerializeField]
    private float moveSpeed = 1f;

    [HideInInspector]
    public int waypointIndex;

    public bool moveAllowed = true;

	// Use this for initialization
	private void Start () 
    {
        waypointIndex = 0;
        transform.position = waypoints[waypointIndex].transform.position;
	}
	
	// Update is called once per frame
	private void Update () 
    {
        if (moveAllowed)
        {
            Move();
        }
    }

    private void Move()
    {
        if (waypointIndex <= waypoints.Length - 1)
        {
            transform.position = Vector2.MoveTowards(transform.position,
            waypoints[waypointIndex].transform.position,
            moveSpeed * Time.deltaTime);
        }
    }
}