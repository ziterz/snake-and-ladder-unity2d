﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChallengeLogic : MonoBehaviour
{
    public GameObject[] Challenge;

    public GameObject challenge1Panel, challenge2Panel, challenge3Panel, challenge4Panel, challenge5Panel;
    // Start is called before the first frame update
    void Start()
    {
        Challenge = GameObject.FindGameObjectsWithTag("Challenge");

        challenge1Panel = GameObject.Find("Challenge1Panel");
        challenge2Panel = GameObject.Find("Challenge2Panel");
        challenge3Panel = GameObject.Find("Challenge3Panel");
        challenge4Panel = GameObject.Find("Challenge4Panel");
        challenge5Panel = GameObject.Find("Challenge5Panel");

        foreach(GameObject panelChallenge in Challenge)
        {
            panelChallenge.gameObject.SetActive(false);
        }
        
        switch(PlayerPrefs.GetInt("Level"))
        {
            case 17:
                challenge1Panel.gameObject.SetActive(true);
            break;

            case 23:
                challenge2Panel.gameObject.SetActive(true);
            break;

            case 30:
                challenge3Panel.gameObject.SetActive(true);
            break;

            case 34:
                challenge4Panel.gameObject.SetActive(true);
            break;

            case 44:
                challenge5Panel.gameObject.SetActive(true);
            break;
        }
    }
}
