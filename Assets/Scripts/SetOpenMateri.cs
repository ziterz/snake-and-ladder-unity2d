using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class SetOpenMateri : MonoBehaviour
{
    public void OnSubmit(int materi)
    {
        PlayerPrefs.SetInt("OpenMateri",materi);
        SceneManager.LoadScene(7);
    }
}
