﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FollowThePath : MonoBehaviour 
{
    public Transform[] waypoints;

    [SerializeField]
    private float moveSpeed = 1f;

    [HideInInspector]
    public int waypointIndex;

    public bool moveAllowed = false;
    public bool upStairAllowed = false, downStairAllowed = false;

    private static GameObject player;

    private int bonus;

	// Use this for initialization
	private void Start () 
    {
        waypointIndex = PlayerPrefs.GetInt("Position");
        Debug.Log("waypointIndex : " + waypointIndex);
        transform.position = waypoints[waypointIndex].transform.position;

        if (PlayerPrefs.GetInt("ChallengeSuccess") == 1)
        {
            if(waypointIndex%10 == 0)
            {
                bonus = Random.Range(5, 10);
                StartCoroutine(DelayUpStair());
            }
            else
            {
                bonus = 11 - (waypointIndex%10) + Random.Range(5, 10);
                StartCoroutine(DelayUpStair());
            }
            
        } else if (PlayerPrefs.GetInt("ChallengeSuccess") == 2)
        {
            
            if(waypointIndex%10 == 0)
            {
                bonus = Random.Range(10, 15);
                StartCoroutine(DelayDownStair());
            }
            else
            {
                bonus = (waypointIndex%10) + Random.Range(5, 10);
                StartCoroutine(DelayDownStair());
            }
        }
	}
	
	// Update is called once per frame
	private void Update () {
        if (moveAllowed)
        {
            Move();
        }

        if (upStairAllowed)
        {
            UpStair();
        }

        if (downStairAllowed)
        {
            DownStair();
        }
	}

    IEnumerator DelayUpStair()
    {
        yield return new WaitForSeconds(0.5f);
        upStairAllowed = true;
    }

    IEnumerator DelayDownStair()
    {
        yield return new WaitForSeconds(0.5f);
        downStairAllowed = true;
    }


    private void Move()
    {
        if (waypointIndex <= waypoints.Length - 1)
        {
            transform.position = Vector2.MoveTowards(transform.position,
            waypoints[waypointIndex].transform.position,
            moveSpeed * Time.deltaTime);

            if (transform.position == waypoints[waypointIndex].transform.position)
            {
                PlayerPrefs.SetInt("Position", waypointIndex);
                PlayerPrefs.SetInt("Status", 1);
                waypointIndex += 1;
            }
        }
    }

    private void UpStair()
    {
        transform.position = Vector2.MoveTowards(transform.position,
        waypoints[waypointIndex+bonus].transform.position,
        moveSpeed * Time.deltaTime);

        if (transform.position == waypoints[waypointIndex+bonus].transform.position)
        {
            PlayerPrefs.SetInt("Position", (waypointIndex+bonus-1));
            waypointIndex = (waypointIndex+bonus-1);

            PlayerPrefs.SetInt("ChallengeSuccess", 0);
            upStairAllowed = false;
        }
    }

    private void DownStair()
    {
        transform.position = Vector2.MoveTowards(transform.position,
        waypoints[waypointIndex-bonus].transform.position,
        moveSpeed * Time.deltaTime);

        if (transform.position == waypoints[waypointIndex-bonus].transform.position)
        {
            PlayerPrefs.SetInt("Position", (waypointIndex-bonus-1));
            waypointIndex = (waypointIndex-bonus-1);

            PlayerPrefs.SetInt("ChallengeSuccess", 0);
            downStairAllowed = false;
            SceneManager.LoadScene(5);
        }
    }

}