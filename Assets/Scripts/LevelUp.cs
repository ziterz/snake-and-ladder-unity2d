﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUp : MonoBehaviour
{
    // Start is called before the first frame update
    public void OnSubmit()
    {
        PlayerPrefs.SetInt("Level",PlayerPrefs.GetInt("Level") + 1);
    }
}
