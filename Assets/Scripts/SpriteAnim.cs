﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteAnim : MonoBehaviour
{
	private SpriteRenderer mySpriteRenderer;

    private void Awake()
   {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
   }

   private void Update()
   {      
        if (PlayerPrefs.GetInt("Position") > 0 && PlayerPrefs.GetInt("Position") <= 10)
        {
            mySpriteRenderer.flipX = false;
        }
        else if (PlayerPrefs.GetInt("Position") > 10 && PlayerPrefs.GetInt("Position") <= 20)
        {
            mySpriteRenderer.flipX = true;
        }
        else if (PlayerPrefs.GetInt("Position") > 20 && PlayerPrefs.GetInt("Position") <= 30)
        {
            mySpriteRenderer.flipX = false;
        }
        else if (PlayerPrefs.GetInt("Position") > 30 && PlayerPrefs.GetInt("Position") <= 40)
        {
            mySpriteRenderer.flipX = true;
        }
        else if (PlayerPrefs.GetInt("Position") > 40 && PlayerPrefs.GetInt("Position") <= 50)
        {
            mySpriteRenderer.flipX = false;
        }
        else if (PlayerPrefs.GetInt("Position") > 50 && PlayerPrefs.GetInt("Position") <= 60)
        {
            mySpriteRenderer.flipX = true;
        }
        else if (PlayerPrefs.GetInt("Position") > 60 && PlayerPrefs.GetInt("Position") <= 70)
        {
            mySpriteRenderer.flipX = false;
        }
        else if (PlayerPrefs.GetInt("Position") > 70 && PlayerPrefs.GetInt("Position") <= 80)
        {
            mySpriteRenderer.flipX = true;
        }
        else if (PlayerPrefs.GetInt("Position") > 80 && PlayerPrefs.GetInt("Position") <= 90)
        {
            mySpriteRenderer.flipX = false;
        }
        else if (PlayerPrefs.GetInt("Position") > 90 && PlayerPrefs.GetInt("Position") <= 100)
        {
            mySpriteRenderer.flipX = true;
        }
    }
}