﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddScore200 : MonoBehaviour
{
    public void OnSubmit()
    {
        switch(PlayerPrefs.GetInt("Level"))
        {
            case 17:
                PlayerPrefs.SetInt("Chapter2", PlayerPrefs.GetInt("Chapter2") + 200);
            break;

            case 23:
                PlayerPrefs.SetInt("Chapter3", PlayerPrefs.GetInt("Chapter3") + 200);
            break;

            case 30:
                PlayerPrefs.SetInt("Chapter4", PlayerPrefs.GetInt("Chapter4") + 200);
            break;

            case 43:
                PlayerPrefs.SetInt("Chapter8", PlayerPrefs.GetInt("Chapter8") + 200);
            break;
        }
    }
}
